
#ifdef __CUDA_ARCH__
#if __CUDA_ARCH__ < 320 
//since shf.l and shf.r instructions (funnelshift) are only defined in compute 3.2 and higher
// we write our own version for lower compute versions
__device__ uint32_t __funnelshift_lc(uint32_t lo, uint32_t hi, uint32_t shift)
{
	return hi << shift | (lo >> (32 - shift));
}
__device__ uint32_t __funnelshift_rc(uint32_t lo, uint32_t hi, uint32_t shift)
{
	return (hi << (32 - shift)) | lo >> shift;
}
#endif
#endif 

//this a logically identical adaption of xoroshiro128+ with values 55, 14, 36 for a, b and c respectively.
//optimised for use with 32 bit integers since the original used 64bit integers however gpus don't natively support 64bit integer operations.
__device__ inline void next(uint4 &state)
{
	uint32_t s1hi = state.x;
	uint32_t s1lo = state.y;
	uint32_t s2hi = state.z;
	uint32_t s2lo = state.w;

	s2lo ^= s1lo;
	s2hi ^= s1hi;

	state.x = ((s1lo << 23) | (s1hi >> 9)) ^ s2hi ^ __funnelshift_lc(s2lo, s2hi, 14);
	state.y = __funnelshift_rc(s1lo, s1hi, 9) ^ s2lo ^ (s2lo << 14);
	state.z = (s2lo << 4) | (s2hi >> 28);
	state.w = __funnelshift_rc(s2lo, s2hi, 28);
}

__device__ inline float randomNumber(uint4 &state)
{
	//get result by adding 32 low bits from s1 and s2 (state y and state w)
	uint32_t randomInt = state.y + state.w;
	float result = (randomInt + 0.5f)/ static_cast<float>(0xffffffff);

	//move to next state
	next(state);

	return result;
}

__device__ inline double getRandomDouble(uint4 &state)
{
	uint64_t s1 = (uint64_t)state.x << 32 | state.y;
	uint64_t s2 = (uint64_t)state.z << 32 | state.w;
	next(state); //we used the whole state so move to the next one
	return ((s1 + s2) + 0.5) / static_cast<double>(0xffffffffffffffff);	
}

__device__ inline uint32_t random32bit(uint4 &state)
{
	uint32_t randomInt = state.y + state.w;

	//move to next state
	next(state);
	return randomInt;
}

__device__ inline uint64_t random64bit(uint4 &state)
{
	uint64_t randomInt = state.y + state.w + ((uint64_t(state.x) + uint64_t(state.z))<<32);

	//move to next state
	next(state);
	return randomInt;
}

__device__ inline float2 randomGuassian(uint4 &state)
{
	//we are using 64 bit integer made up of concatenating two 32 bit ints
	//we then convert this to a float so the float will represent a number between 1 and 1/2^64. (this is required for the distribution to be valid over Z=6.66)
	//however since the floating point number only has 24 bits of precision, if the first 32 bit
	//integer is greater than 8388608 any additional bits of precision would be lost so we can just multiply
	//the first int by 2^32 and use the second int for the next random number.
	float rand1, rand2;
	uint32_t randInt = state.x + state.z;
	if(randInt + 1U < 8388609U)//this checks if the randInt is less than 8388608 (in which case the lower bits matter)
							   //or if randInt is equal to 2^32 - 1 in which case the addition of the lower bits will cause wrap around
							   //which will make them matter
	{
		uint64_t s1 = (uint64_t)state.x << 32 | state.y;
		uint64_t s2 = (uint64_t)state.z << 32 | state.w;
		rand1 = ((s1 + s2) + 0.5f) / static_cast<float>(0xffffffffffffffff);
		next(state); //we used the whole state so move to the next one
	}
	else
	{
		//The low 32 bits added between 0 and 2 to the high 32 bits with a mean of 1 (12.5% 0, 75% 1 12.5% 2)
		//the small discontinuity at 2^23/2^32 means that an interval containing just the float 0.001953125 has only 87.5% the probability it should
		//and the interval containing just the float 0.00195312523 has 112.5% the probability it should, the interval containing both 
		//points has the correct probability. this disparity is dwarfed by the effect of float precision as the float approaches 1.0.
		rand1 = (randInt + 0.5f) / static_cast<float>(0xffffffff);
	}

	rand2 = ((int32_t)(state.y + state.w)) / static_cast<float>(0x80000000);//this rolls the multiplication by 2 into the division by 2^32 and gives us a value between -1 and 1.
	next(state);

	float r = sqrtf(-2.0f * logf(rand1));//smallest possible value = 0.000345, then 0, this leads to peaks in the probability corresponding to these values
	
	float2 result;
	//due to finite floating point precision there are slight irregularities around the distribution very close to zero
	//this exists in the interval [-1.5e-6,+1.0e-6] using theta in [0, 2PI]. using theta in [-PI, PI] instead reduces this interval
	//to [-7e-7,+7e-7] and leads to greater symmetry around the y axis in this interval.
	sincospif(rand2, &(result.x), &(result.y));
	result.x *= r;
	result.y *= r;
	return result;
}

__device__ inline float2 randomGuassian(float2 mean, float2 stddv, uint4 &state)
{
	float2 result = randomGuassian(state);
	result.x = result.x*stddv.x + mean.x;
	result.y = result.y*stddv.y + mean.y;
	return result;
}

__device__ inline float randomLaplace(uint4 &state)
{
	float R = randomNumber(state)-0.5f;
	return copysignf(logf(1.0f - fabsf(R)), R);
}
